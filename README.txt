Description:
    Script to track objects of different color and store the data (cordinates) of each objects
    in a JSON file with details filled with id, color, x-cordinate, y-cordinate, date-time stamp
    and scan count.  

Version:
	v1.1

Platform:
    Windows.
    Tested on windows 10.

Enviroment:
    Google chrome. (works offline)

Programming languages:
    Html,CSS,Javascript.

Excecution file:
    Color_tracking_realease_V1/index.html

Dependencies:
    tracking-min.js
    FileSaver.js
    script.js
    style.css

Updates:
	In the previous version, the data of each object was stored in each line one after the other 
	irrespective of any parameter (ie, id, color. etc ). Now, considering id as a parameter, each
	line in the output file represents all the objects of one perticular id. 

	


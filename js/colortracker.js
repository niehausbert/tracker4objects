// Color Tracker Code of Javascript library
// Wait for the page to be ready

var tracker,trackingTask;
var maxdist = 60;           // threshold distance
var sim_mode = 0;           // simulation mode =0, realtime mode=1
var old_raw= [];           // old objects array
var dist = [];             // distance array
var running = 0;             // this reflects the image frame number once started
var x=0,y=0,col,row;      // x,y pixel coordinates of image and the matrix row colomn it is stored into
var i = 0, count = 1;         // general counting variables
var lastid=0;              // track of id to be allotted next
var archive=[];            // storage array
var col4tracker = ["yellow","cyan","magenta"]; // colors that should be tracked
var track_data = {
  "camera": {
    "width":640,
    "height":480
  },
  "tracklist": []
};

// for debugging in simulation mode populate tracklist
var simu_track_list = [
  {
			"id": "00003",
			"color":"red",
			"track":[
				[20,244,934589355],
				[25,241,934593799],
				[32,249,940023002]
			]
    },
    {
			"id": "00001",
			"color":"cyan",
			"track":[
				[120,24,682340423],
				[102,29,683294899],
				[122,32,683400034],
				[132,49,683442334]
			],
    },
    {
			"id":"00002",
			"color":"yellow",
			"track":[
				[120,24,682340423],
				[102,29,683294899],
				[122,32,683400034],
				[132,49,683442334]
			]
		}
  ];

// In simulation mode populate tracklist in track_data
if (sim_mode == 1) {
  track_data.tracklist = simu_track_list;
}

function el(id) {
  return document.getElementById(id);
}

//function init_tracker()
window.addEventListener("load",function()
{ //for each new page loaded this event is triggered
  console.log("Page loaded!");

  console.log("Set color values");
  for (var i = 0; i < col4tracker.length; i++) {
    document.getElementById("tColor"+(i+1)).value = col4tracker[i];
  };
  // "offset" is used to move circles and ids on the canvas.
  // Necessary due to menu that is dynamic in the responsive design,
  // Without 'offset' the recognized circles are displayed out of the camera area.
  var offset = {
      "x": 0,
      "y": 180
  };
  // Grab reference to the tags we will be using
  var canvas  = el('canvas'); // canvas onto which annotations can be made on top of image
  var context = canvas.getContext('2d'); //reference handle for operations on this canvas

  // Create the color tracking object
  //tracker = new tracking.ColorTracker(col4tracker); // setting pallate of colors to track
  tracker = new tracking.ColorTracker(["yellow","cyan","magenta"]); // setting pallate of colors to track

  // Add callback for the "track" event
  tracker.on('track', function(e)  //handler function to be executed each time new page is loaded (new image frame)
  { context.clearRect(0, 0, canvas.width, canvas.height); //clearing the canvas and the circles drawn
    if (e.data.length !== 0) // if new image has object entries
    {  if(sim_mode==1)         // Simulation mode [testing purpose]
      { if (running==0) {var new_raw=[];simulation0();running++; }  //executes for the first incoming frame
        else  // executes for all the other frames
         { var new_raw=[]; // clear and create new raw for every new image frame
          for (var i = 0; i<4;i++)
          { var obj_t=new Track_Point("yellow",old_raw[i].x+i,old_raw[i].y+i,dateTime,lastid); //Creating new object with (color,x,y,date,id)
            new_raw.push(obj_t); //push the new created object to new_raw array
          }
          mapids(new_raw,old_raw); //function call to map the old ids to new ids.
          running++; // increment processed image frame number
        }
      }
      else
      // Realtime mode
      {  if(running==0)   //executes for the first incoming frame
          { running++;    // increment processed image frame number
            e.data.forEach(function(rect)     //Function call for each object in the frame
            { color=rect.color; x=(rect.x + (rect.width/2)); y=(rect.y + (rect.height/2));d=DateTime();  //Creating new object with (color,x,y,date,id)
              old_raw.push(new Track_Point(color,x,y,d,lastid));lastid++;     //push the new created object to new_raw array
            });
          }
          else
          { var new_raw = [];     // clear and create new raw for every new image frame
            e.data.forEach(function(rect)     //Function call for each object in the frame
            { color=rect.color; x=(rect.x + (rect.width/2)); y=(rect.y + (rect.height/2));d=DateTime();  //Creating new object with (color,x,y,date,id)
              new_raw.push(new Track_Point(color,x,y,d,lastid));        //push the new created object to new_raw array
            });
            mapids(new_raw,old_raw);    //function call to map the old ids to new ids.
            running++;    // increment processed image frame number
          }
      }
      drawcirc(old_raw, context,offset);     //function to draw a circle on detected object.
      archive.push(JSON.stringify(old_raw));    // Store the data into the storage (archive array)
      // Changes: Push JSON instead of string to archive - .
      //archive.push(old_raw);    // Store the data into the storage (archive array)
    }
  });
});

//window.addEventListener("load", init_tracker);



/////Apcog funcions/////

/**
 * Gives the date time stamp when the object is being tracked.
 */

function DateTime(){
  var d=new Date();           // date time stamp
  var date =  d.getDate();var month = d.getMonth()+1;var year = d.getFullYear();var hour = d.getHours();var minutes = d.getMinutes();var seconds = d.getSeconds();var milliseconds = d.getMilliseconds();
  var dateTime = date +"/"+ month +"/"+ year +"  "+ hour +":"+ minutes +":"+ seconds +":"+ milliseconds;    //combining all the variable together to get the desired format.
  return dateTime;
}

/**
 * Draw a colored circle on the canvas ie, drawing a circle on the detected object with ids.
 * @param  {Array} draw_raw The array of objects.
 * @param  {canvas} context Place where the circles are drawn.
*/

function drawcirc(draw_raw, context, offset) {
  var xoff = offset.x;
  var yoff = offset.y;
  var color = {r:255, g:255, b:255};    // color defined in r,g,b
  context.beginPath(); context.font = "20px Arial";     // font size of text
  radius=20;    //circle to be drawn with defined radius
  for (var k=0;k<draw_raw.length;k++)
  {
    context.strokeStyle = "rgb(" + color.r + ", " + color.g + ", " + color.b + ")";   //color for strokestyle.
    context.beginPath();
    context.arc(draw_raw[k].x + xoff, draw_raw[k].y + yoff, radius , 0, 2 * Math.PI); //draw circle.
    context.stroke();
    context.fillStyle = "black";    //text color
    context.fillText(draw_raw[k].id,draw_raw[k].x-10+xoff,draw_raw[k].y+10+yoff);   //write text.

  }
}


function toggleTracking(pbutton) {
  var state = pbutton.getAttribute("status") || "off";
  console.log("Status: "+state);
  switch (state) {
    case "off":
        console.log("Switch ON Tracking");
        pbutton.setAttribute("status","on");
        pbutton.innerHTML = "Stop Tracking";
        startTracking();
        $("#iconplay").show()
        $("#iconpause").hide()
    break;
    default:
        console.log("Switch OFF Tracking");
        pbutton.setAttribute("status","off");
        pbutton.innerHTML = "Start Tracking";
        stopTracking();
        $("#iconpause").show()
        $("#iconplay").hide()
  }
}
/**
 * Function to start the tracking process. This function is called when the "start tracking"
 * button is pressed in the interface.
*/

function startTracking(){
  trackingTask = tracking.track(webcam, tracker, { camera: true, fps:10 });  //initiating the tracking
}

/**
 * Function to stop the tracking process
 */

function stopTracking(){
  trackingTask.stop();    //stopping the tracking process.
  var canvas  = el('canvas');    // canvas onto which annotations can be made on top of image.
  var context = canvas.getContext('2d');    //reference handle for operations on this canvas.
  // stored the observed camera image properties in "track_data"
  //track_data.camera.width = canvas.width;
  //track_data.camera.height = canvas.height;
  context.clearRect(0, 0, canvas.width, canvas.height);   //clearing the canvas
  //console.log("IN RUNNING : ",trackingTask.inRunning())

}

function delete_track_data() {
  archive = [];
  i = 0;
  count = 1;         // general counting variables
  lastid=0;              // track of id to be allotted next
  track_data.tracklist = [];
  document.getElementById('app_editor').value='';
  app.nav.page('edit');
}


function track_id_exists(pid,ptrack_list) {
  var id_found = -1;
  //console.log("CALL: track_id_exists(pid,ptrack_list) started");
  //console.log("CHECK: ptrack_list.length = " +  ptrack_list.length);
  if (ptrack_list) {
    for (var i = 0; i < ptrack_list.length; i++) {
      if (ptrack_list[i].id === pid) {
        id_found = i;
      }
    }
  } else {
    console.error("ERROR: ptrack_list does not exist");
  }
  /*
  if (id_found < 0) {
    console.log("ID '" + pid + "' not found in track list");
  }
  */
  return id_found;
}



function split_track_points(ptp_string) {
    var j = null;
    var vJSON = null;
    try {
          j = JSON.parse(ptp_string);
      } catch(e) {
          alert("ERROR Track Point Array parsing JSON: "+e);
      }
      // Do something with the parsed JSON
      if (j) {
          vJSON = j;
          //console.log("CALL: split_track_point!");
      } else {
        console.error("ERROR: in parse_archive_arr() parsing JSON failed!");
      }
      return vJSON;
}

function tp_clean(tp) {
  // clean up of track point attributes in tp befor pushing to track
  delete tp.color;
  delete tp.id;
  delete tp.scan_count;
  delete tp.map;
  // track point is now cleaned
  return tp;
}

function assign2track(ptp,ptrack_list) {
  //console.log("START: assign2track(ptp,ptrack_list)");
  if (ptp.hasOwnProperty("id")) {
    //console.log("id of ptp is '" + ptp.id + "' and id exists");
    var pos = track_id_exists(ptp.id,ptrack_list);
    if (pos >= 0) {
      //console.log("Track[" + ptp.id + "] exists at array pos " + pos);
    } else {
      //console.log("Track[" + ptp.id + "] created");
      var new_track = {
        "id": ptp.id,
        "color":ptp.color,
        "track":[]
      };
      //console.log("Push new track to ptrack_list="+JSON.stringify(ptrack_list,null,4));
      ptrack_list.push(new_track);
      //console.log("New track pushed");
      pos = ptrack_list.length - 1;
      //console.log("Set pos="+pos);
      //console.log("Assign TrackPoint ["+ptrack_list.length+"] to Track " + ptp.id +".");
    }
    // push track point in parameter ptp to track array
    (ptrack_list[pos]).track.push(tp_clean(ptp));
  } else {
    console.error("ERROR: ptp.id does not exist in call of assign2track(ptp,ptrack_list)");
  }
}

function assign_trackarray(ptp_arr,ptrack_list) {
  for (var i = 0; i < ptp_arr.length; i++) {
    //console.log("Assign [" + i + "]");
    assign2track(ptp_arr[i],ptrack_list);
  }
}

function assign_points2tracks(parchive,ptrack_list) {
  console.log("STARTED: assign_points2tracks(parchive,ptrack_list)");
  var track_list = ptrack_list || [];
  //var id_arr = get_track_id_array(parchive);
  //console.log("id_arr=('" + id_arr.join("','") + "')");
  for (var i = 0; i < parchive.length; i++) {
    var tp_arr = split_track_points(parchive[i]);
    //console.log("Split: "+parchive[i]+" length: " + tp_arr.length);
    assign_trackarray(tp_arr,track_list);
  }
  return track_list;
}

function trackdata2archive(ptrack_data) {
  var track_list = ptrack_data.tracklist;
  var arch = [];
  var count_i = 0;
  var track = null;
  var idlist = ["x","y","color"]
  var trackpoint = {
    "id":0,
    "color":0,
    "x":0,
    "y":0,
    "scan_count":0
  };
  for (var i = 0; i < track_list.length; i++) {
    // iterate over all tracks in track_list
    trackpoint.id = track_list[i].id;
    trackpoint.color = track_list[i].color;
    track = track_list[i].track;
    for (var k = 0; k < track.length; k++) {
      // append all trackpoint in track to archive
      count_i++;
      for (var key in idlist) {
        if (track.hasOwnProperty(key)) {
          trackpoint[key] = track[k][key];
        }
      }
      trackpoint.scan_count = count_i;
      arch.push(JSON.stringify(trackpoint));
    }
  }
  return arch;
}

function generateTracks4JSON(id) {
  console.log("Generate Tracks in textarea with id='" + id + "'");
  var node = el(id);
  if (node) {
    console.log("DOM Node for textarea with id='" + id + "' exists!");
    track_data.tracklist = assign_points2tracks(archive,track_data.tracklist);
    node.value = JSON.stringify(track_data,null,4);
    alert("Track List assigned - DONE!");
  } else {
    console.error("ERROR: textarea with id '" + id + "' is not defined in the DOM.");
  }
}


function parse_archive_arr(parchive) {
    console.log("CALL: assign_points2tracks(parchive)");
    archive = parchive;
    count = parchive.length;
    track_data.tracklist = assign_points2tracks(parchive,track_data.tracklist);
}

function parse_loaded_json(pfile) {
  var j = null;
  try {
        j = JSON.parse(pfile);
    } catch(e) {
        alert("JSON ERROR: "+e);
    }

    // Do something with the parsed JSON
    if (j) {
       if (Array.isArray(j)) {
         console.log("loaded JSON is of type track 'archive'.");
         archive = j;
         console.log("CALL: assign_points2tracks(archive)");
         track_data.tracklist = assign_points2tracks(archive,track_data.tracklist);
         console.log("CALL: parse_archive_arr(pfile) and 'archive' set with loaded file!");
       } else {
         console.log("loaded JSON is of type 'JSON track_data'");
         track_data = j;
         archive = trackdata2archive(j);
       }
    } else {
      console.error("ERROR: in parse_loaded_json() parsing JSON failed!");
    }
}

/* This is the structure of the tracking data in the specs
{
    camera: {
    width:640,
    height:480
  },
  {
      "id": "00003",
      "color":"red",
      "track":[
        [20,244,934589355],
        [25,241,934593799],
        ...
      ]
    },
    {
      "id": "00001",
      "color":"cyan",
      "track":[
        [120,24,682340423],
        ...
        ],
    },
    {
      "id":"00002",
      "color":"yellow",
      "track":[
        [120,24,682340423],
        ...
      ]
    }
  ]
}

*/
function savejson() {
  console.log("CALL: savejson() "+JSON.stringify(archive,null,4));
  var content = JSON.stringify(archive,null,4);
  var filename = "track_archive.json";
  filename = add_time_stamp2filename(filename);

  saveFile2HDD(filename,content);
  //archive=[];   //clear archive once the file is saved.
}

/**
 * Funtion to save the data to file in JSON format. This funtion obtains the stored data from
 * storage (archive[]) and write it into the file.
 */
 function saveTracks4JSON(id)
 {
   console.log("CALL: saveTracks4JSON()");
   // id of the textarea DOM element to store the track data "track_data" in.
   var content = "tracking data undefined";
   if (track_data) {
     track_data.tracklist = assign_points2tracks(archive,track_data.tracklist);
     content = JSON.stringify(track_data,null,4);
     var filename = el("tFilename").value;
     filename = add_time_stamp2filename(filename);
     saveFile2HDD(filename,content);
   } else {
     console.error("ERROR: tracking data 'track_data' does not exist");
   }
   //archive=[];   //clear archive once the file is saved.
 }


function saveJSON() {
  console.log("CALL: saveJSON()");
  var nextid=0;
  var opt = [];
  var track_point_arr = null;
  var track_point = null;
  var color = "";
  for(nextid=0;nextid<lastid;nextid++) {
    // archive is an array strings
    for (var n in archive) {
      var track_point_arr =JSON.parse(archive[n]);
      // archive[n] is a stringified array of track points
      for (m=0;m<track_point_arr.length;m++) {
        track_point = track_point_arr[m];
        color = track_point.c;
        var obj_id=(track_point.id);
        if (obj_id==nextid){
          opt.push(JSON.stringify(archive1[m]));
        }
      }
    }
    opt.push("\n");
  }
  var blob = new Blob(opt, {type: "text/json;charset=utf-8"});
  saveAs(blob,"archive_" +d+ ".json");
  //archive=[];   //clear archive once the file is saved.
}



function savejson_old() {
  console.log("CALL: savejson()");
  var nextid=0;
  var opt = [];
  for(nextid=0;nextid<lastid;nextid++) {
    for (var n in archive) {
      var archive1=JSON.parse(archive[n]);
      var length_archive=(archive1.length);
      for (m=0;m<length_archive;m++)
      {
        var obj_id=(archive1[m].id);
        if (obj_id==nextid){
          opt.push(JSON.stringify(archive1[m]));
        }
      }
    }
    opt.push("\n");
  }
  var blob = new Blob(opt, {type: "text/json;charset=utf-8"});
  saveAs(blob,"archive_" +d+ ".json");
  //archive=[];   //clear archive once the file is saved.
}


/**
 * Function to map ids from old_raw to new_raw..
 * @param  {Array} new_raw_a The array of new objects.
 * @param  {Array} old_raw_a The array of old objects.
 * Effected Arrays : dist[], new_raw[], old_raw[], sorting_array[]
 * Effected Variables: rindx,cindx,ref_indx,indx,found.
*/

function mapids(new_raw_a,old_raw_a){
  //get_distance function gives the distance between each object in new_raw to all
  //the objects in old_raw.
  //It can be imagined as 2d matrix with rows(new_raw) and columns(old_raw),but the
  //representaion in done in 1 dimensional array.
  var dist=get_distance(old_raw_a,new_raw_a);  var ref_indx=0;
  var newlen= new_raw_a.length;   //gives the length of the new_raw_a array.
  for (var rindx = 0; rindx<newlen; rindx++)  //picking values for each index in new_raw.
  {   var sorting_array=[]; var oldlen = old_raw_a.length; var checkcol=0;  //declaring a sorting_array; obtaining the length of the old_raw_a
      for(var cindx =0;cindx<oldlen;cindx++)      //picking values for each index in old_raw.
      { ref_indx=(rindx * oldlen)+cindx;    // one dimensional representation of imagined 2d array index.
          //Obtaining all the old distances for one new point and store it in sorting_array, if the map property is '0'.
          if(dist[ref_indx].map==0){ sorting_array[checkcol]=(dist[ref_indx]);checkcol++;}
      }
      sorting_array.sort(function(a,b){return a.distance-b.distance}); //Sort the sorting_array in ascending order
      var indx=0; var refdist=0; var z=0; var testindx=0; var found = 0; var sort_len=sorting_array.length;
      while ((found==0)&&(indx<sort_len)) //initailizing found with 0 and if indx is less than sorting_array length.
      {   if(sorting_array[indx].map != 1)   // pick elements which are not mapped in dist array.
          //taking each element in sorting array and put it in refdist.
          //compare the refdist with all the old distances in dist array that refers to the new point.
          { refdist=sorting_array[indx].distance; var ref_col=sorting_array[indx].colindx;
              for(var j =0;j<newlen;j++)
              { z = parseInt((j * oldlen)+(parseInt(ref_col))); // one dimensional representation of imagined 2d array index.
                  // if a value is found less than refdist then break and go to the next element in sorting_array.
                  if(dist[z].distance <refdist ) { testindx= -1; break;}
              }
              if(testindx != -1)
              {  //if the refdist is smallest after comparsion then allot the old_id of
                  // sorting_array to id of new_raw_a element at index "rindx"
                  if(refdist<= maxdist)
                  {   new_raw_a[rindx].id=sorting_array[indx].old_id;old_raw_a[indx].map=1;new_raw_a[rindx].map=1;
                      for(var m =0;m<newlen;m++) {var z1 = parseInt((m * oldlen)+(parseInt(ref_col)));dist[z1].map=1;}
                      found=1; //make found flag 1.
                  }
              }
          }
      indx=indx + 1;
      }
      //if there are still points after comparison the we consider it as new points
      // and allot them with new ids.
      if (found==0) { new_raw_a[rindx].id=lastid; lastid+=1;new_raw_a[rindx].map=1;}
  }
  // clear the old_raw array and copy elements of new_raw_a array to old_raw array.
  for(var pop_val=0;pop_val<old_raw.length;pop_val++){old_raw.pop();}
  for (var value=0;value<new_raw_a.length;value++){old_raw[value]=new_raw_a[value];old_raw[value].scan_count=running;}
  //clear new_raw_a array for new elements to fill in.
  for(var pop_val=0;pop_val<new_raw_a.length;pop_val++){new_raw_a.pop();}

}

/**
 * This is the consructor for the new object.
 * @param {string} c    color of the object
 * @param {Float } x    x-cordinate of the object
 * @param {Float } y    y-cordinate of the object
 * @param {Date  } d    date-time stamp
 * @param {int   } id   unique id for the object
 */

function Track_Point(c,x,y,d,id)
{
  // Constructor for a track point - Capitalize Letter
  this.id=id; this.color=c; this.x=x; this.y=y;
  this.d=d; this.map=0; this.scan_count=0;
}

/**
 * Constructor for new Distance_Obj used in get_distance function.
 * @param {Float} distance  distance between two objects (old and new).
 * @param {int} old_id      unique id of the old objects.
 * @param {int} new_id      unique id of the new objects.
 * @param {int} rowindx     index of the row in dist array (imaginary).
 * @param {int} colindx     index of the column in dist array(imaginary).
 */

function Distance_Obj (distance,old_id,new_id,rowindx,colindx)
{
  // Constructor for Distance Object - capitalize function name - EN
  this.distance=distance; this.old_id=old_id; this.new_id=new_id;  this.new_id=new_id;
  this.map=0; this.rowindx=rowindx; this.colindx=colindx;
}

/**
 * Function to calculate the distance between two points and returns a distance matrix[basically an array].
 * @param  {Array} old_raw_d The array of old objects.
 * @param  {Array} new_raw_d The array of new objects.
 * @return {Array} delta     Array of distances between two objects.
 * Effected Arrays : delta[]
*/

function get_distance( old_raw_d, new_raw_d)
{
  // calculate distance between track points and return a delta-array
  var delta=[];
  var ans;var next=0;
  //distance between points of new_raw_d and old_raw_d and put the result in delta array.
  if((old_raw_d.length>0)&&(new_raw_d.length>0))
  { for (var ro = 0; ro<new_raw_d.length ; ro++)  //for loop for allobjects in new_raw_d
    {
      for(var colm=0; colm<old_raw_d.length;colm++)   //for loop for all objects in old_raw_d
      { next=(ro * old_raw_d.length)+colm;  // incrementing variable index in delta array
        // distance=sqrt((x1-x2)^2+(y1-y2)^2)
        ans=(Math.sqrt((Math.pow((old_raw_d[colm].x - new_raw_d[ro].x),2))+(Math.pow((old_raw_d[colm].y - new_raw_d[ro].y),2))));   //distance calclulation between two objects
        var temp = new Distance_Obj(ans,old_raw_d[colm].id,new_raw_d[ro].id,ro,colm);
        delta[next]= temp;    //populating delta array.
      }
    }
  }
  return delta;   //returning delta array to where this function is called.
}

/**
 * This function is used for testing purpose. Any random values can be given for the x-cordinate
 * and y cordinate for checking the output.
 */

function simulation0()
{
    if(running==0)
    {   //random values given to objects for test "Track_Point(color,x,y,date,id)"
        old_raw[0]=new Track_Point("yellow",10,40,d,lastid) ;lastid++;
        old_raw[1]=new Track_Point("yellow",100,140,d,lastid);lastid++;
        old_raw[2]=new Track_Point("yellow",50,120,d,lastid); lastid++;
        old_raw[3]=new Track_Point("yellow",200,50,d,lastid);lastid++;
    }
}

/*////////////// Assumptions //////////////

1]  2d Array for dist[]
      Here, it is assumed that the distance between two objects are store dist array in
      the form of 2d array.

2]  Row and Column
      In dist array each row represents new objects and each column represents old objects.
      Basically each row represents the distance between one new object to all the other
      old objects.

//////// Aspects to be taken care of //////

1]  Printing objects in a array:
      Printing the whole array of objects at once is not recommended. It may show the length
      of array but not values. Hence it is better to print each object of an array in a loop.

2]  2d array handling:
      2d Array handling in javascript seems to be immature. When 2d array is created with index
      i and j, it is difficult to find the value at any location[i,j]. For example [4x4]matrix,
      It may give all the values in the last row or column, even when you travrse through
      whole matrix and give the length of the array as 4.

////////////////////////////////////////////*/
